from django import forms
from .models import User
from django.core.exceptions import ValidationError


class RegistrationForm(forms.Form):
    """
    Implementation of Registration form
    """
    email = forms.CharField(max_length=50, widget=forms.TextInput(), required=True)
    password1 = forms.CharField(max_length=50, widget=forms.PasswordInput(), required=True, label='Password')
    password2 = forms.CharField(max_length=50, widget=forms.PasswordInput(), required=True, label='Password confirm')

    def clean_email(self):
        """
        validator of email form field
        """
        email = self.cleaned_data['email']
        if '@' not in email:
            raise ValidationError(u"\u2022 " + 'You forget "@" in you email')
        if '.' not in email:
            raise ValidationError(u"\u2022 " + 'Probably you forgot to specify your email domain like ".com"')
        if User.objects.filter(email=email).exists():
            raise ValidationError(u"\u2022 " + "The email address you've chosen is already registered.")
        else:
            return email

    def clean_password2(self):
        """
        validator of password form field
        """
        password1 = self.cleaned_data['password1']
        password2 = self.cleaned_data['password2']
        if password1 != password2:
            raise ValidationError(u"\u2022 " + "Passwords don't match")
        else:
            return password2

    def save_new_user(self):
        """
        RegistrationForm object method allowed create User model object and save it into database
        """
        user = User.objects.create(email=self.cleaned_data['email'], password=self.cleaned_data['password2'])
        return user


class LoginForm(forms.Form):
    """
    Implementation of Login form
    """
    email = forms.CharField(max_length=50, widget=forms.TextInput(), required=True)
    password = forms.CharField(max_length=50, widget=forms.PasswordInput(), required=True)

    def clean_email(self):
        """
        validator of email form field
        """
        email = self.cleaned_data['email']
        if '@' not in email:
            raise ValidationError(u"\u2022 " + 'You forget "@" in you email')
        elif '.' not in email:
            raise ValidationError(u"\u2022 " + 'Probably you forgot to specify your email domain like ".com"')
        elif not User.objects.filter(email=email).exists():
            raise ValidationError(u"\u2022 " + "This email address is not registered.")
        else:
            return email

    def clean_password(self):
        """
        validator of password form field
        """
        try:
            email = self.cleaned_data['email']
            user = User.objects.get(email=email)
            password = self.cleaned_data['password']
            if user.password != password:
                raise ValidationError(u"\u2022 " + "Wrong password")
            else:
                return password
        except Exception:
            raise ValidationError(u"\u2022 " + "Wrong password")

    def get_user_id(self):
        """
        LoginForm object method allowed get exists User object from database
        """
        email = self.cleaned_data['email']
        user_obj = User.objects.get(email=email)
        return user_obj

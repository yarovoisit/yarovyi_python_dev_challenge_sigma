from django.db import models


class User(models.Model):
    """
    Implementation of User account model in DB which allow to create User objects
    """
    email = models.EmailField(max_length=50, blank=False, unique=True)
    password = models.CharField(max_length=50, blank=False)

from django.shortcuts import render, redirect
from .forms import LoginForm, RegistrationForm
from django.views.generic import View


class RegistrationView(View):
    """
    render Registration page/form on ".../registration/" page
    """

    def get(self, request):
        """
        render Registration page when request method is GET
        """
        form = RegistrationForm()
        return render(request, "account/registration.html", context={'form': form})

    def post(self, request):
        """
        render Registration page when request method is POST
        """
        bound_form = RegistrationForm(request.POST)
        if bound_form.is_valid():
            new_user = bound_form.save_new_user()
            request.session['user_id'] = new_user.id
            return redirect('my_lists')
        else:
            return render(request, "account/registration.html", context={'form': bound_form})


class LoginView(View):
    """
    render Login page/form on index page
    """

    def get(self, request):
        """
        render Login page when request method is GET
        """
        if request.session.get('user_id', False):
            return redirect('my_lists')
        else:
            form = LoginForm()
            return render(request, "account/login.html", context={'form': form})

    def post(self, request):
        """
        render Login page when request method is POST
        """
        bound_form = LoginForm(request.POST)
        if bound_form.is_valid():
            request.session['user_id'] = bound_form.get_user_id().id
            return redirect('my_lists')
        else:
            return render(request, "account/login.html", context={'form': bound_form})


def logout(request):
    """
    implementation of reaction on button "LOGOUT".
    Manually can calling by ".../logout/" page
    """
    if request.session.get('user_id', False):
        del request.session['user_id']
    return redirect('login')

from django.apps import AppConfig


class ReachedGoalsConfig(AppConfig):
    name = 'reached_goals'

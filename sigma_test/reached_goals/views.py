from django.shortcuts import render


def estimate(request):
    return render(request, 'reached_goals/estimate.html')

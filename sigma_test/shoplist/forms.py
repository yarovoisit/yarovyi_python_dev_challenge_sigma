from django import forms
from .models import ShoppingList, Item
from django.db.utils import IntegrityError


class ShoppingListCreationForm(forms.Form):
    """
    Implementation of ShoppingListCreation form
    """
    list_name = forms.CharField(max_length=30, widget=forms.TextInput(attrs={'placeholder': 'Add new list'}))
    user_id = forms.IntegerField(widget=forms.HiddenInput())

    def save_new_list(self, list_name, user_obj):
        """
        ShoppingListCreationForm object method allowed create ShoppingList model object and save it into database
        """
        new_list = ShoppingList.objects.create(list_name=list_name, user_id=user_obj)
        return new_list


class AddItemCreationForm(forms.Form):
    """
    Implementation of AddItemCreation form
    """
    item_name = forms.CharField(max_length=30, widget=forms.TextInput(attrs={'placeholder': ' Add new item'}))

    def save_new_item(self, list_id, item_name):
        """
        AddItemCreationForm object method allowed create Item model object and save it into database
        """
        if not Item.objects.filter(name=item_name).exists():
            new_item = Item.objects.create(name=item_name)
        else:
            new_item = Item.objects.get(name=item_name)
        if new_item in ShoppingList.objects.get(id=list_id).items.all():
            raise IntegrityError
        ShoppingList.objects.get(id=list_id).items.add(new_item)
        return new_item

from django.db import models


class ShoppingList(models.Model):
    """
    Implementation of ShoppingList model in DB which allow to create shopping list objects
    """
    list_name = models.CharField(max_length=30)
    user_id = models.ForeignKey('account.User', on_delete=models.CASCADE)

    class Meta:
        unique_together = (('list_name', 'user_id'),)


class Item(models.Model):
    """
    Implementation of Item model in DB which allow to create item objects
    """
    name = models.CharField(max_length=30, unique=True)
    shopping_lists = models.ManyToManyField('ShoppingList', blank=True, related_name='items')

from django.shortcuts import render, redirect
from .models import ShoppingList
from django.views.generic import View
from .forms import ShoppingListCreationForm, AddItemCreationForm
from django.apps import apps
from django.db.utils import IntegrityError
from django.core.paginator import Paginator


class ListOfShoppingLists(View):
    """
    render list of shopping lists on ".../my_lists" page
    """

    def get(self, request):
        """
        render list of shopping lists page when request method is GET
        """
        user_id = request.session['user_id']
        shopping_lists = ShoppingList.objects.filter(user_id=user_id).order_by('-id')
        paginator = Paginator(shopping_lists, 10)
        page_number = request.GET.get('page', 1)
        page = paginator.get_page(page_number)
        form = ShoppingListCreationForm()
        return render(request,
                      "shoplist/my_lists.html",
                      context={'form': form,
                               'user_id': user_id,
                               'shopping_lists': page})

    def post(self, request):
        """
        render list of shopping lists page when request method is POST
        """
        user_id = request.session['user_id']
        shopping_lists = ShoppingList.objects.filter(user_id=user_id).order_by('-id')
        list_name = request.POST['list_name']
        User = apps.get_model('account', 'User')
        user_obj = User.objects.get(id=request.session['user_id'])
        paginator = Paginator(shopping_lists, 10)
        page_number = request.GET.get('page', 1)
        page = paginator.get_page(page_number)
        bound_form = ShoppingListCreationForm({'list_name': list_name, 'user_id': user_obj.id})
        if bound_form.is_valid():
            try:
                bound_form.save_new_list(list_name=bound_form.cleaned_data['list_name'], user_obj=user_obj)
                return redirect('my_lists')
            except IntegrityError:
                error = 'This list is already exists, please try another name !!!'
                return render(request,
                              "shoplist/my_lists.html",
                              context={'form': bound_form,
                                       'user_id': user_id,
                                       'shopping_lists': page,
                                       'error': error})


def delete_my_shopping_list(request, shopping_list_id):
    """
    implementation of reaction on delete list button.
    Manually can calling by ".../delete/list_<id>/" page
    """
    shopping_list = ShoppingList.objects.get(id=shopping_list_id)
    shopping_list.delete()
    return redirect('my_lists')


class ConcreteShoppingList(View):
    """
    render concrete shopping list on ".../my_lists/<id>" page
    """

    def get(self, request, shopping_list_id):
        """
        render concrete shopping list page when request method is GET
        """
        concrete_shopping_list = ShoppingList.objects.get(id=shopping_list_id)
        items = concrete_shopping_list.items.order_by('-id').all()
        paginator = Paginator(items, 10)
        page_number = request.GET.get('page', 1)
        page = paginator.get_page(page_number)
        form = AddItemCreationForm()
        return render(request,
                      'shoplist/my_shopping_list.html',
                      context={'items': page,
                               'list_name': concrete_shopping_list.list_name,
                               'shopping_list_id': shopping_list_id,
                               'form': form})

    def post(self, request, shopping_list_id):
        """
        render concrete shopping list page when request method is POST
        """
        concrete_shopping_list = ShoppingList.objects.get(id=shopping_list_id)
        items = concrete_shopping_list.items.order_by('-id').all()
        paginator = Paginator(items, 10)
        page_number = request.GET.get('page', 1)
        page = paginator.get_page(page_number)
        bound_form = AddItemCreationForm(request.POST)
        if bound_form.is_valid():
            try:
                bound_form.save_new_item(list_id=shopping_list_id, item_name=bound_form.cleaned_data['item_name'])
                return redirect('shopping_list', shopping_list_id)
            except IntegrityError:
                error = 'This item is already exists in current list !!!'
                return render(request,
                              "shoplist/my_shopping_list.html",
                              context={'items': page,
                                       'list_name': concrete_shopping_list.list_name,
                                       'shopping_list_id': shopping_list_id,
                                       'form': bound_form,
                                       'error': error})


def delete_element_from_shopping_list(request, shopping_list_id, element_id):
    """
    implementation of reaction on delete item button.
    Manually can calling by ".../delete/list_<id>/element_<id>/" page
    """
    item = ShoppingList.objects.get(id=shopping_list_id).items.get(id=element_id)
    ShoppingList.objects.get(id=shopping_list_id).items.remove(item)
    return redirect('shopping_list', shopping_list_id)

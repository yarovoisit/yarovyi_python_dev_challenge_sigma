"""sigma_test URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path
from account import views as account_views
from shoplist import views as shoplist_views
from reached_goals import views as reached_goals

urlpatterns = [
    path('', account_views.LoginView.as_view(), name='login'),
    path('logout/', account_views.logout, name='logout'),
    path('registration/', account_views.RegistrationView.as_view(), name='registration'),
    path('admin/', admin.site.urls),
    path('my_lists/', shoplist_views.ListOfShoppingLists.as_view(), name='my_lists'),
    path('my_lists/<int:shopping_list_id>/', shoplist_views.ConcreteShoppingList.as_view(), name='shopping_list'),
    path('delete/list_<int:shopping_list_id>/', shoplist_views.delete_my_shopping_list, name='delete_shopping_list'),
    path('delete/list_<int:shopping_list_id>/element_<int:element_id>/',
         shoplist_views.delete_element_from_shopping_list, name='delete_element'),
    path('estimate/', reached_goals.estimate, name='estimate'),
]
